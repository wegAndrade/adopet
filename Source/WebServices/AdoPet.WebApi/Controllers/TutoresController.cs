﻿using AdoPet.Application.Commands.Tutores;
using AdoPet.Application.Queries;
using AdoPet.Domain.Models;
using AdoPet.WebApi.Dto_s;
using MediatR;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AdoPet.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutoresController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<TutoresController> _logger;
        private readonly ITutorQuery _tutorQuery;
        public TutoresController(IMediator mediator, ILogger<TutoresController> logger, ITutorQuery tutorQuery)
        {
            _mediator = mediator;
            _logger = logger;
            _tutorQuery = tutorQuery;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var tutores = await _tutorQuery.GetAsync();
                if (tutores is null || tutores.Count() == 0)
                    return NotFound();
                return Ok(tutores);
            }
            catch (Exception ex)
            {

                _logger.LogError($"Erro ao consultar tutores {ex.Message} ");
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{IdTutor}")]
        public async Task<IActionResult> Get([FromRoute] int IdTutor)
        {
            try
            {
                if (IdTutor == 0)
                    return BadRequest("IdTutor Inválido");
                var tutor = await _tutorQuery.GetAsync(IdTutor);

                if (tutor is null)
                    return NotFound();
                return Ok(tutor);
            }
            catch (Exception ex)
            {

                _logger.LogError($"Erro ao consultar tutores {ex.Message} ");
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TutorRequest request)
        {
            _logger.LogInformation("Iniciando criação de tutor");
            try
            {
                var command = new CreateTutorCommand(request.Nome, request.Email, request.Senha);

                var response = await _mediator.Send(command);

                _logger.LogInformation($"Fim criação de tutor: {response}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($@"Erro ao gravar tutor: {request}\n Erros: {ex.Message} ");
                return StatusCode(500, ex);
            }
        }


        [HttpPut("{IdTutor}")]
        [HttpPatch("{IdTutor}")]
        public async Task<IActionResult> PutPatch([FromBody] TutorRequest request, [FromRoute] int IdTutor)
        {
            _logger.LogInformation("Iniciando atualização de tutor {0}", IdTutor);
            try
            {
                if (IdTutor == 0)
                    return BadRequest("IdTutor Inválido");
                var command = new UpdateTutorCommand(IdTutor, request.Nome, request.Email, request.Senha);

                var response = await _mediator.Send(command);

                _logger.LogInformation($"Fim atualização de tutor: {response}");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao atualizar tutor: {IdTutor} \n erros: {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{IdTutor}")]
        public async Task<IActionResult> Delete([FromRoute] int IdTutor)
        {
            _logger.LogInformation("Iniciando deleção de tutor {0}", IdTutor);
            try
            {
                if (IdTutor == 0)
                    return BadRequest("IdTutor Inválido");
                var command = new DeleteTutorCommand(IdTutor);

                await _mediator.Send(command);

                _logger.LogInformation($"Fim deleção de tutor: {IdTutor}");

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao deletar tutor: {IdTutor}  \n erros: {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

    }
}
