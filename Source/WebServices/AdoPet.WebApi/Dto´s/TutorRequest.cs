﻿namespace AdoPet.WebApi.Dto_s
{
    public record TutorRequest
    {
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
    }
}
