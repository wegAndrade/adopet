﻿using AdoPet.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace AdoPet.Application.Queries
{
    public class TutorQuery : ITutorQuery
    {
        private readonly AdoPetDbContext _dbContext;

        public TutorQuery(AdoPetDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Responses.TutorResponse?>> GetAsync()
        {
            var tutores= await _dbContext.Tutores.AsNoTracking().ToListAsync();

            return tutores.Select(t => Responses.TutorResponse.FromModel(t)).ToList();
        }

        public async Task<Responses.TutorResponse?> GetAsync(int Id)
        {
           var tutor = await  _dbContext.Tutores.AsNoTracking().FirstOrDefaultAsync(f => f.Id == Id);

            return Responses.TutorResponse.FromModel(tutor);
        }
    }
}
