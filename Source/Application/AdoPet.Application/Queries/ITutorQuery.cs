﻿namespace AdoPet.Application.Queries
{
    public interface ITutorQuery
    {
        Task<IEnumerable<Responses.TutorResponse?>> GetAsync();
        Task<Responses.TutorResponse?> GetAsync(int Id);
    }
}
