﻿using AdoPet.Application.Commands.Tutores;
using AdoPet.Application.Responses;
using AdoPet.Domain.Models;
using AdoPet.Domain.Notifications;
using AdoPet.Infra.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AdoPet.Application.CommandHandlers.Tutores
{
    public class DeleteTutorCommandHandler : IRequestHandler<DeleteTutorCommand, Unit>
    {
        private readonly NotificationContext _notificationContext;
        private readonly AdoPetDbContext _dbContext;

        public DeleteTutorCommandHandler(NotificationContext notificationContext, AdoPetDbContext dbContext)
        {
            _notificationContext = notificationContext;
            _dbContext = dbContext;
        }

        public async Task<Unit> Handle(DeleteTutorCommand request, CancellationToken cancellationToken)
        {
            if (request.Invalid)
            {
                _notificationContext.AddNotifications(request.ValidationResult);
                return Unit.Value;
            }

            Tutor tutor = await _dbContext.Tutores.FirstOrDefaultAsync(t => t.Id == request.Id);

            if (tutor == null)
            {
                _notificationContext.AddNotification("Bad Request", $"Tutor não encontrado Id: {request.Id}");
                return Unit.Value;

            }

            _dbContext.Tutores.Remove(tutor);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
