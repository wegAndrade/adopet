﻿using AdoPet.Application.Commands.Tutores;
using AdoPet.Application.Responses;
using AdoPet.Domain.Models;
using AdoPet.Domain.Notifications;
using AdoPet.Infra.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AdoPet.Application.CommandHandlers.Tutores
{
    public class CreateTutorCommandHandler : IRequestHandler<CreateTutorCommand, TutorResponse?>
    {
        private readonly NotificationContext _notificationContext;
        private readonly AdoPetDbContext _dbContext;
        public CreateTutorCommandHandler(NotificationContext notificationContext, AdoPetDbContext dbContext)
        {
            _notificationContext = notificationContext;
            _dbContext = dbContext;
        }



        public async Task<TutorResponse?> Handle(CreateTutorCommand request, CancellationToken cancellationToken)
        {
            if (request.Invalid)
            {
                _notificationContext.AddNotifications(request.ValidationResult);
                return null;
            }

            if (await _dbContext.Tutores.AnyAsync(t => t.Email == request.Email))
            {
                _notificationContext.AddNotification("Bad Request",$"Email: {request.Email} já cadastrado");
                return null;
            }

            Tutor tutor =  new Tutor() { Email= request.Email, Nome = request.Nome, Senha = request.Senha };


            await _dbContext.Tutores.AddAsync(tutor, cancellationToken);

            await _dbContext.SaveChangesAsync();

            return TutorResponse.FromModel(tutor);
        }
    }
}
