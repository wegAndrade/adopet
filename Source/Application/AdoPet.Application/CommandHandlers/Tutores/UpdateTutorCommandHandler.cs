﻿using AdoPet.Application.Commands.Tutores;
using AdoPet.Application.Responses;
using AdoPet.Domain.Models;
using AdoPet.Domain.Notifications;
using AdoPet.Infra.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AdoPet.Application.CommandHandlers.Tutores
{
    public sealed class UpdateTutorCommandHandler : IRequestHandler<UpdateTutorCommand, TutorResponse?>
    {

        private readonly NotificationContext _notificationContext;
        private readonly AdoPetDbContext _dbContext;

        public UpdateTutorCommandHandler(NotificationContext notificationContext, AdoPetDbContext dbContext)
        {
            _notificationContext = notificationContext;
            _dbContext = dbContext;
        }

        public async Task<TutorResponse?> Handle(UpdateTutorCommand request, CancellationToken cancellationToken)
        {
            if (request.Invalid)
            {
                _notificationContext.AddNotifications(request.ValidationResult);
                return null;
            }

            if (_dbContext.Tutores.Count(t => t.Email == request.Email) > 1)
            {
                _notificationContext.AddNotification("Bad Request", $"Email: {request.Email} já cadastrado");
                return null;
            }


            if (!await _dbContext.Tutores.AnyAsync(t => t.Id == request.Id))
            {
                _notificationContext.AddNotification("Bad Request", $"Tutor não encontrado Id: {request.Id}");
                return null;

            }
            var tutor = new Tutor()
            {
                Email = request.Email,
                Nome = request.Nome,
                Senha = request.Senha,
                Id = request.Id,
            };

            _dbContext.Entry<Tutor>(tutor).State = EntityState.Detached;
            _dbContext.Tutores.Update(tutor);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return TutorResponse.FromModel(tutor);
        }
    }
}
