﻿using FluentValidation;
using FluentValidation.Results;
using MediatR;
namespace AdoPet.Application.Commands
{
    public abstract class CommandBase<TResponse> : IRequest<TResponse>
    {
        public Guid Id { get; protected set; }
        public bool Valid { get; private set; }
        public bool Invalid => !Valid;
        public ValidationResult ValidationResult { get; private set; }

        public void Validate<TModel>(TModel model, AbstractValidator<TModel> validator)
        {
            ValidationResult = validator.Validate(model);
            Valid = ValidationResult.IsValid;
        }

     
     
    }
}
