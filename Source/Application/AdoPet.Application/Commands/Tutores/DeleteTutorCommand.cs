﻿using AdoPet.Application.Validators;
using MediatR;

namespace AdoPet.Application.Commands.Tutores
{
    public sealed class DeleteTutorCommand: CommandBase<Unit>
    {
        public int Id { get; set; }

        public DeleteTutorCommand(int id)
        {
            Id = id;
            Validate(this, new DeleteTutorCommandValidator());
        }
    }
}
