﻿using AdoPet.Application.Responses;
using AdoPet.Application.Validators;
using MediatR;

namespace AdoPet.Application.Commands.Tutores
{
    public sealed class CreateTutorCommand : CommandBase<TutorResponse?>
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

        public CreateTutorCommand(string nome, string email, string senha)
        {
            Nome = nome;
            Email = email;
            Senha = senha;
            Validate(this, new CreateTutorValidator());
        }
    }
}
