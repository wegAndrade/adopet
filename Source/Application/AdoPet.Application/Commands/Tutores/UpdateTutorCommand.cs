﻿using AdoPet.Application.Responses;
using AdoPet.Application.Validators;

namespace AdoPet.Application.Commands.Tutores
{
    public sealed class UpdateTutorCommand: CommandBase<TutorResponse?>
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

        public UpdateTutorCommand(int id, string nome, string email, string senha)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Senha = senha;
            Validate(this, new UpdateTutorCommandValidator());
        }
    }
}
