﻿using AdoPet.Domain.Models;

namespace AdoPet.Application.Responses
{
    public sealed record TutorResponse
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }

        public TutorResponse(int id, string nome, string email)
        {
            Id = id;
            Nome = nome;
            Email = email;
        }

        internal static TutorResponse? FromModel(Tutor tutor)
        {
            if (tutor is null)
                return null;
            return new TutorResponse(tutor.Id,tutor.Nome, tutor.Email);
        }


    }
}
