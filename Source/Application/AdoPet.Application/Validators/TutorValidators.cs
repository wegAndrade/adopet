﻿using AdoPet.Application.Commands.Tutores;
using FluentValidation;

namespace AdoPet.Application.Validators
{
    public class CreateTutorValidator : AbstractValidator<CreateTutorCommand>
    {
        public CreateTutorValidator()
        {

            RuleFor(c => c.Nome).NotEmpty()
                .WithMessage("Nome é obrigatorio");

            RuleFor(c => c.Email).NotEmpty()
              .WithMessage("Email é obrigatorio");

            RuleFor(c => c.Email).EmailAddress()
              .WithMessage("Email Inválido");


            RuleFor(c => c.Senha).NotEmpty()
              .WithMessage("Senha é obrigatoria");
        }
    }

    public class DeleteTutorCommandValidator : AbstractValidator<DeleteTutorCommand>
    {
        public DeleteTutorCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty()
                .WithMessage("Id deve ser informado para exclusão");
        }
    }

    public class UpdateTutorCommandValidator : AbstractValidator<UpdateTutorCommand>
    {
        public UpdateTutorCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty()
                .WithMessage("Id deve ser informado para atualização");

            RuleFor(c => c.Nome).NotEmpty()
                .WithMessage("Nome é obrigatorio");

            RuleFor(c => c.Email).NotEmpty()
              .WithMessage("Email é obrigatorio");

            RuleFor(c => c.Email).EmailAddress()
              .WithMessage("Email Inválido");


            RuleFor(c => c.Senha).NotEmpty()
              .WithMessage("Senha é obrigatoria");
        }
    }
}
