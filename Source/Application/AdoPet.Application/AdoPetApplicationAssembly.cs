﻿using System.Reflection;

namespace AdoPet.Application
{
    public static class AdoPetApplicationAssembly
    {
        public static Assembly GetAssembly => typeof(AdoPetApplicationAssembly).Assembly;   
    }
}
