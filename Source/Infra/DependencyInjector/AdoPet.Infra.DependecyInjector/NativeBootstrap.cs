﻿using AdoPet.Application.Queries;
using AdoPet.Domain.Notifications;
using AdoPet.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace AdoPet.Infra.DependecyInjector
{
    public static class NativeBootstrap
    {
        public static IServiceCollection AddDi(this IServiceCollection services, IConfiguration configuration, Assembly assembly)
        {
            services.AddDbContext<AdoPetDbContext>(opt => opt.UseSqlServer(configuration.GetConnectionString("Default"), b => b.MigrationsAssembly("AdoPet.WebApi")),ServiceLifetime.Scoped);
            services.AddScoped<AdoPetDbContext>();

            services.AddScoped<NotificationContext>();

            services.AddScoped<ITutorQuery, TutorQuery>();

            services.AddMediatR(options =>
            {
                options.RegisterServicesFromAssembly(assembly);
            });

            return services;
        }
    }
}