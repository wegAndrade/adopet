﻿using AdoPet.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AdoPet.Infra.Data.Mappings
{
    public sealed class TutorMap : IEntityTypeConfiguration<Tutor>
    {
        public void Configure(EntityTypeBuilder<Tutor> builder)
        {
            builder.ToTable(nameof(Tutor).ToUpper());
            builder.HasKey(t => t.Id);
            
        }
    }
}
