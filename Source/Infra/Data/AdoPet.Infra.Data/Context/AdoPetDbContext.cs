﻿using AdoPet.Domain.Models;
using AdoPet.Infra.Data.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoPet.Infra.Data.Context
{
    public sealed class AdoPetDbContext: DbContext
    {
        public AdoPetDbContext(DbContextOptions<AdoPetDbContext> options) : base(options) { }

        public DbSet<Tutor> Tutores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new TutorMap());
        }
    }
}
